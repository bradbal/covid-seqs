#Helper functions called to help facilitate the SAR-CoV2 analysis.

#For converting three letter residue code to one letter
res_three_to_one = {'GLY':'G','ALA':'A','PRO':'P','VAL':'V','LEU':'L','ILE':'I',
					'MET':'M','PHE':'F','TYR':'Y','TRP':'W','SER':'S','THR':'T',
					'CYS':'C','ASN':'N','GLN':'Q','LYS':'K','HIS':'H','ARG':'R',
					'ASP':'D','GLU':'E',
					'NAG':'', #NAG is for peptidoglycan, ignore
					'BMA': '', #BMA - beta-D-mannose, ignore
					'ZN': '', ' ZN':'', #Zinc ion, ignore
					'CL': '', ' CL':'', #Chloride ion, ignore
				 	'HOH':'' #water molecule, ignore
					}
