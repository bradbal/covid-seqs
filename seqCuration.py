#Purpose of this script is to download all of the relevant sars sequence data.
import os
os.chdir('/Users/uqbbalde/Desktop/Uni_Studies/projects/sarsCov2/')

import matplotlib.pyplot as plt
import utilities
import fasta
import sequence
import guide
import ete3

# Read in the FASTA file
full_record = utilities.load_sequences("sarsCov2_S_Protein_Seqs.fasta")

#Removing sequences with X content
x_removed_records = fasta.exclude_character(full_record, "X")

#Determining length distribution ...
seqs = list(x_removed_records.values())
lengths = [len(seq.seq) for seq in seqs]
plt.hist(lengths)
plt.show()

filtered = fasta.subset_records(records=x_removed_records, length=800)

#Pulling more detailed sequence information ....
seqObjs = []
for name in filtered:
	seq = str(filtered[name].seq)
	seq = sequence.Sequence(seq, name=name,
							alphabet=sequence.predefAlphabets['ProteinwX'])
	#seq = guide.getSequence(name, 'genbank', guide.Protein_Alphabet,format='')
	seqObjs.append(seq)

sequence.writeFastaFile('sarsCov2_curated.fasta', seqObjs)

# Create an alignment using MAFFT.

#Now reading in the alignment and converting to PHYLYP format for input to PHyML
records = utilities.SeqIO.parse("sarsCov2.aln", "fasta")
count = utilities.SeqIO.write(records, "sarsCov2.phylip", "phylip")

#Now need to make sure that the sequences in the fast file correspond to the
#labels in the tree...
seqs = sequence.readFastaFile('sarsCov2.aln')
treeFile = open('sarscov2_phylip_phyml/sarscov2_phylip_phyml_tree.txt', 'r')
treeString = [line for line in treeFile][0]
tree = ete3.Tree(treeString)

###############################################################################
#Now running sequence curation for the more high-quality data downloaded
#from NGDC
files = os.listdir('./data/')
fastas = [sequence.readFastaFile(f'./data/{fastaFile}', sequence.Protein_Alphabet_wX)
		  for fastaFile in files]
seqs = []
for fastaSeqs in fastas:
	seqs.extend(fastaSeqs)

#Checking sequence lengths
seqLens = [len(seq) for seq in seqs]
plt.hist(seqLens)
plt.show()

#Similar distribution to the above, but sequences with high lengths, just taking
#these. Also removing any with X content, since GRASP dosn't like this.
seqs = [seq for seq in seqs if len(seq)>1000 and 'X' not in seq.sequence]
#348 in total, more than what I had with the blast search
#342 if you remove sequences with X content.

#Now extracting the protein sequence of SARS-CoV from the PDB file along with
#the secondary structure and functional information.
import Bio.PDB as pdb
#See https://biopython.org/wiki/The_Biopython_Structural_Bioinformatics_FAQ
import helper_functions

parser = pdb.PDBParser()
structure = parser.get_structure('SARS-S', 'SARS_CoV_Structure.pdb')

#Don't include the peptidoglycan in the protein sequence.
pdbSeq = ''.join([helper_functions.res_three_to_one[res.resname]
				  for res in structure.get_residues()])

#Creating new sequence object...
seq = sequence.Sequence(pdbSeq, name='SARS-CoV', info=structure.header['name'],
						alphabet=sequence.Protein_Alphabet)
seqs.append(seq)
#Now 343 sequences with the structure-derived sequence

#Writing these out ....
sequence.writeFastaFile('corona_S_proteins.fasta', seqs)

#Now reading in the alignment and converting to PHYLYP format for input to PHyML
records = utilities.SeqIO.parse("corona_S_proteins.aln", "fasta")
count = utilities.SeqIO.write(records, "corona_S_proteins.phylip", "phylip")

#Reading in the aligned sequences...
seqs = sequence.readFastaFile('corona_S_proteins.aln', sequence.Protein_wGAP)

#Taking a look at the tree...
treeFile = open('corona_s_proteins_phylip_phyml/corona_s_proteins_phylip_phyml_tree.txt',
				'r')
treeString = [line for line in treeFile][0]
tree = ete3.Tree(treeString)

#Removing the sequences that didn't remove above ...
seqNames = [seq.name for seq in seqs]
for leaf in tree.get_leaves():
	if leaf.name not in seqNames:
		leaf.delete()

#Removing any seqs not in the tree...
treeNames = [leaf.name for leaf in tree.get_leaves()]
seqsFiltered = [seq for seq in seqs if seq.name in treeNames]

#Writing out the alignment and tree ....
sequence.writeFastaFile('corona_S_proteins_treeMatched.aln', seqsFiltered)

#Now we need to root the tree....
# Calculate the midpoint node
midpoint = tree.get_midpoint_outgroup()
# and set it as tree outgroup
tree.set_outgroup(midpoint)

#Now saving as a newick string....
tree.write(format=1, outfile="corona_S_proteins_tree.nwk")
